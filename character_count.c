/* Character Count
 * Drew McDermott
 * adm75@pitt.edu
 * 
 * Accepts 1 argument, a string of lenght k
 * if k<1 or k is not a power of 2 an error is returned
 * 
 * Counts the number of letters in a string
 * 
 * Compile with -lm flags for math.h
 */ 


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>

typedef struct args
{
	int s;
	int e;
} args;
typedef struct alpha
{
	int arr[26];
} alpha;

int dispRes(alpha *in)
{
	int i = 0;
	int val;
	alpha res = *in;
	for(i = 0; i<26; i++)
	{
		val = res.arr[i];
		if(val == 0) continue;
		char c = i + 65;
		printf("%c : %d\n",c,val);
	}
}

int forkToCounter(alpha *res, char send[3])
{
	int pipefd[2];
	int ret[2];
	const char* tosend = send;
	char pipefdR[25];
	char pipefdW[25];
	
	
	
	if (pipe(pipefd) == -1)
	{
		fprintf(stderr, "Pipe Failure\n");
		return -1;
	}
	
	int a = snprintf(pipefdR, 24, "%d", pipefd[0]);
	int b = snprintf(pipefdW, 24, "%d", pipefd[1]);
	
	pid_t p = fork();
	
	if(p < 0)
	{
		
	}
	else if (p == 0)
	{
		
		execl("./counter",tosend,pipefdR,pipefdW,NULL);
		return 0;
	}
	

	/* Close write fd */
	close(pipefd[1]);

    /* Read data */
	read(pipefd[0],ret, 2*sizeof(int));
	
	/* close read */
	close(pipefd[0]);
	
	//update res
     if(ret[0] < 26)
     {
	(*res).arr[ret[0]] = 1;
	}
	 if(ret[1] < 26)
     {
	(*res).arr[ret[1]] = 1;
	}

}

int sendArgs(int pipefd[2], args *in)
{
	/* close read fd */
	close(pipefd[0]);
	
	/* Write data */
	write(pipefd[1], in, sizeof(args));
	
	/* close write */
	close(pipefd[1]);
}

int recvArgs(int pipefd[2], args *out)
{
	/* Close write fd */
	close(pipefd[1]);

    /* Read data */
	read(pipefd[0],out , sizeof(args));
	
	/* close read */
	close(pipefd[0]);
}

int sendRes(int pipefd[2], alpha *in)
{
	/* close read fd */
	close(pipefd[0]);
	
	/* Write data */
	write(pipefd[1], in, sizeof(alpha));
	
	/* close write */
	close(pipefd[1]);
}

int recvRes(int pipefd[2], alpha *out)
{
	/* Close write fd */
	close(pipefd[1]);
 

    /* Read data */
	read(pipefd[0],out, sizeof(alpha));

	
	/* close read */
	close(pipefd[0]);
}

int recur_fork(char phrase[], int start, int end, int parpipe[2])
{
	
	
	int pipefd[2][2][2];
	/* 
	 * 1: Child 0/1
	 * 2: toChild = 0, toParent = 1
	 * 3: read/write as used by pipe
	 */
	 //Loop vars
	 int i = 0;
	 int j = 0;
	 int k = 0;
	 
	 for(j = 0; j < 2; j++)
	 {
		 for(k = 0; k < 2;k ++)
		 {
			if (pipe(pipefd[j][k]) == -1)
			{
				fprintf(stderr, "Pipe Failure\n");
				return -1;
			} 
		 }		 
	 }
	
	args childArgs[2];
	args myArgs;
	alpha childRes[2];
	alpha myRes;

	int l = abs(end - start) + 1;
	int s1 = start;
	int e1 = start + (l/2) - 1;
	int s2 = start + (l/2);
	int e2 = end;
	
			
		childArgs[0].s = s1;
		childArgs[0].e = e1;
		
		//printf("SET S:%d E:%d\n",childArgs[0].s,childArgs[0].e);
		
		childArgs[1].s = s2;
		childArgs[1].e = e2;
	
	pid_t pidL = 0;
	pid_t pidR = 0;
	
	///////////////////////////////////// LEFT
	

	//fork
	pidL = fork();
		
	if(pidL < 0)
	{
		fprintf(stderr, "Fork Failure\nFork:%d-%d",start,end);
		return -1;
	}
	else if (pidL == 0)
	{
		//Child

		recvArgs(pipefd[0][0], &myArgs);

			
		
		
		if(abs(myArgs.e - myArgs.s) <= 1)
		{
			//Base condition
			
			
			for(i = 0; i < 26; i++)
			{
				myRes.arr[i] = 0;
				
			}
			
			
			char send[3];
			
			send[0] = phrase[myArgs.s];
			send[1] = phrase[myArgs.e];
			send[2] = 0;
			//execute count
			forkToCounter(&myRes,send);
					
			//Pipe results
			sendRes(pipefd[0][1],&myRes);
			
			
			return 0;
		}
		else
		{
			//Recursive Call
			//printf("REC S:%d E:%d\n",myArgs.s,myArgs.e);		
			recur_fork(phrase, myArgs.s, myArgs.e, pipefd[0][1]);
			
			return 0;
								
		}
		
	}
	else
	{
		sendArgs(pipefd[0][0], &childArgs[0]);
		
		//read results
		
		recvRes(pipefd[0][1], &childRes[0]);
		
	}
	
	////////////////////////////////////// RIGHT
	
	
	//fork
	pidR = fork();
	
	if(pidR < 0)
	{
		fprintf(stderr, "Fork Failure\nFork:%d-%d",start,end);
		return -1;
	}
	else if (pidR == 0)
	{
		//Child
		recvArgs(pipefd[1][0], &myArgs);
		
		myArgs = (args)myArgs;
		
		
		if(abs(myArgs.e - myArgs.s) <= 1)
		{
			
			//Base condition
			
			for(i = 0; i < 26; i++)
			{
				myRes.arr[i] = 0;
				
			}
			

			char send[3];
			
			send[0] = phrase[myArgs.s];
			send[1] = phrase[myArgs.e];
			send[2] = 0;
			//execute count
			forkToCounter(&myRes,send);
			//Get count results
			
			//Pipe results
			sendRes(pipefd[1][1],&myRes);
			
			return 0;
		}
		else
		{
			
			//Recursive Call
			
			
			recur_fork(phrase, myArgs.s, myArgs.e, pipefd[1][1]);
			
			return 0;
								
		}
		
	}
	else
	{
		//Parent
		
		sendArgs(pipefd[1][0], &childArgs[1]);
		
		//read results
		
		recvRes(pipefd[1][1], &childRes[1]);
		
	}
	
	//Combine results

	for(i =0; i < 26; i++)
	{
		myRes.arr[i] = childRes[0].arr[i] + childRes[1].arr[i];
	}
	if(parpipe != NULL)
	{
	
	sendRes(parpipe,&myRes);
	}
	else
	{
	printf("%s =\n",phrase);
	dispRes(&myRes);
	}
	
	return 0;
	
}


int main(int argc, char *argv[])
{
	char test[] = "ABBCCRFDDGHLZDZA";
	int len = 0;
	
	if(argc < 2)
	{
		printf("No args, using defualt:\n");
		recur_fork(test,0,15, NULL);
	}
	else
	{
		len = strlen(argv[1]);
		double loglen = log2(len);
		if(loglen - (int)loglen == 0)
		{
			//Pow 2
			recur_fork(argv[1],0,len - 1, NULL);
		}
		else
		{
			fprintf(stderr, "%d is not a power of 2\n", len);
			return -1;
		}
	}
	
	
}
