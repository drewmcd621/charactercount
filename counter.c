/*Counter
 * Drew McDermott
 * adm75@pitt.edu
 * 
 * Counts characters
 * Takes 4 arguments
 * 
 * 1: Char array
 * 3: pipefd[0]
 * 4: pipefd[1]
 * 
 * Pipes back 2 integers corrosponding to the posistion in the alphabet
 */ 
 
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int sendRes(int pipefd[2], int *res, int size)
{
	/* close read fd */
	close(pipefd[0]);
	
	/* Write data */
	write(pipefd[1], res, size*sizeof(int));
	
	/* close write */
	close(pipefd[1]);
}

int main(int argc, char *argv[])
{
	if(argc < 3) {
		fprintf(stderr, "Too Few Arguments: %d < 3 \n",argc);
		return -1;
	}

	int pipefd[2];
	pipefd[0] = atoi(argv[1]);
	
	pipefd[1] = atoi(argv[2]);
	
	int c[2];
	
	c[0] = argv[0][0];
	c[1] = argv[0][1];

	
	int i = 0;
	for(i=0; i < 2; i++)
	{
		//lower case
		if(c[i] > 96)
		{
			c[i] -= 97;
		}
		else if(c[i] > 64)
		{
			//Upper case
			c[i] -= 65;
		}
	}
	//debug

	sendRes(pipefd, c, 2);
	
	return 0;
	
}
